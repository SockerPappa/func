
import math

# Koeningsegg Agera R max speed is 440 km/h
agera_r_ms = 440

print("Ett varv runt jordens ekvator tar: " + str(round(6371*2*math.pi // agera_r_ms)) + " timmar och "  + str(round((6371*2*math.pi % agera_r_ms) / agera_r_ms*60)) + " minuter.")
print("Ett varv runt månens ekvator tar: " + str(round(1738*2*math.pi // agera_r_ms)) + " timmar och "  + str(round((1738*2*math.pi % agera_r_ms) / agera_r_ms*60)) + " minuter.")
print("Ett varv runt mars ekvator tar: " + str(round(3396*2*math.pi // agera_r_ms)) + " timmar och "  + str(round((3396*2*math.pi % agera_r_ms) / agera_r_ms*60)) + " minuter.")

def planet_tid(planet, distans, speed = 440):
    timmar = round(distans*2*math.pi // speed)
    minuter = round((distans*2*math.pi % speed) / speed*60)
    return "Ett varv runt %s om man reser i %d km/h tar cirka %d timmar och %d minuter"%(planet, speed, timmar, minuter)
    

